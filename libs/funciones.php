<?php
session_start();
function debuggear($element){
    echo "<pre>";
    print_r($element);
    echo "</pre>";
    exit;
}

function formUsuario(int $tipoUsuario, string $tarea, $titulo = '', $datos = null) {
    $boton = "Registrar";
    if($tipoUsuario == 2){ // Registro de usuario nuevo
        $nombre = "Ingrese su nombre";
        $apellido = "Ingrese su apellido";
        $dni = "Ingrese su DNI (ej. 12345678A)";
        $email = "Ingrese su correo electrónico";
        $direccion = "Ingrese su dirección";
        $password = "Ingrese una contraseña";
    } elseif ($tipoUsuario == 1 && $tarea == "nuevo"){ // Registro de empleado nuevo
        $nombre = "Nombre";
        $apellido = "Apellido";
        $dni = "DNI";
        $email = "Email";
        $direccion = "Dirección";
        $password = "Contraseña";
    }
    if(!isset($nombre)){
        $nombre = '';
        $apellido = '';
        $dni = '';
        $email = '';
        $direccion = '';
        $password = '';
    }
    if (isset($datos)) {
        $vNombre = $datos['nombre'];
        $vApellido = $datos['apellido'];
        $vEmail = $datos['email'];
        $vDireccion = $datos['direccion'];
        $vDni = $datos['dni'];
    } else {
        $vNombre = "";
        $vApellido = "";
        $vEmail = "";
        $vDireccion = "";
        $vDni = "";
    }
    $form = <<<FORM
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                    <h3>$titulo</h3>
                </div>
                <div class="card-body"><!--  Formulario empleado -->
                    <form action="../libs/registroUsuario.php" method="POST" id="formUsuario">
                        <!--INICIO CAMPO NOMBRE DE FORMULARIO-->
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g fill='#FFFFFF'><path d="M16.14 12.33C17.28 11.24 18 9.71 18 8c0-3.31-2.69-6-6-6S6 4.69 6 8c0 1.71.72 3.24 1.86 4.33C3.52 13.05 0 14.94 0 18c0 4 1 4 12 4s12 0 12-4c0-3.06-3.52-4.95-7.86-5.67z"></path></g></svg>							</span>
                            </div>
                            <input type="text" name="nombre" class="form-control" id="inputNombre" maxlength="30" placeholder="$nombre" value="$vNombre" required>
                        </div>
                        <!--FIN CAMPO NOMBRE DE FORMULARIO-->
                        <!--INICIO CAMPO APELLIDO DE FORMULARIO-->
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g fill='#FFFFFF'><path d="M16.14 12.33C17.28 11.24 18 9.71 18 8c0-3.31-2.69-6-6-6S6 4.69 6 8c0 1.71.72 3.24 1.86 4.33C3.52 13.05 0 14.94 0 18c0 4 1 4 12 4s12 0 12-4c0-3.06-3.52-4.95-7.86-5.67z"></path></g></svg>							</span>
                            </div>
                            <input type="text" name="apellido" class="form-control" id="inputApellido" maxlength="30" placeholder="$apellido" value="$vApellido" required>
                        </div>
                        <!--FIN CAMPO APELLIDO DE FORMULARIO-->
                        <!--INICIO CAMPO DNI DE FORMULARIO-->
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g fill='#FFFFFF'><path d="M22 4H2c-.6 0-1 .4-1 1v14c0 .4.3.8.6.9.1.1.3.1.4.1h20c.6 0 1-.4 1-1V5c0-.6-.4-1-1-1zM8 5h8c.5 0 1 .5 1 1s-.5 1-1 1H8c-.5 0-1-.5-1-1s.5-1 1-1zM3 5c.6 0 1 .4 1 1s-.4 1-1 1-1-.4-1-1 .4-1 1-1zm19 13c0 .6-.4 1-1 1H3c-.6 0-1-.4-1-1V9c0-.6.4-1 1-1h18c.6 0 1 .4 1 1v9zM21 7c-.6 0-1-.4-1-1s.4-1 1-1 1 .4 1 1-.4 1-1 1zM10.5 12h-5c-.3 0-.5-.2-.5-.5s.2-.5.5-.5h5c.3 0 .5.2.5.5s-.2.5-.5.5zM10.5 14h-5c-.3 0-.5-.2-.5-.5s.2-.5.5-.5h5c.3 0 .5.2.5.5s-.2.5-.5.5zM10.5 16h-5c-.3 0-.5-.2-.5-.5s.2-.5.5-.5h5c.3 0 .5.2.5.5s-.2.5-.5.5z"></path><circle cx="16.5" cy="13.5" r="2.5"></circle></g></svg>							</span>
                            </div>
                            <input type="text" name="dni" class="form-control" id="inputDni" placeholder="$dni" value="$vDni" pattern="^(\d{8})([A-Z]{1})$" title="ej. 12345678M" required>
                            <input type="hidden" name="dniOriginal" value="$vDni">
                        </div>
                        <!--FIN CAMPO DNI DE FORMULARIO-->			
                        <!--INICIO CAMPO EMAIL--->
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g fill='#FFFFFF'><path d="M1 5v14l7-7-7-7zm22 14V5l-7 7 7 7zm-11-3l-3.5-3.5L1 20h22l-7.5-7.5L12 16zM1 4l11 11L23 4H1z"></path></g></svg></i>
                                </span>
                            </div>
                            <input type="email" name="email" class="form-control" id="inputEmail" placeholder="$email" value="$vEmail" required>
                        </div>
                        <!--FIN CAMPO EMAIL-->
                        <!--INICIO CAMPO DIRECCIÓN DE FORMULARIO-->
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g fill='#FFFFFF'>
                                    <g>
                                        <path d="M12,3l-9.5,9.5c-0.3,0.3-0.3,0.7,0,1l0,0c0.3,0.3,0.7,0.3,1,0L12,5c0,0,8.5,8.5,8.5,8.5c0.3,0.3,0.7,0.3,1,0v0
                                            c0.3-0.3,0.3-0.7,0-1L12,3z"></path>
                                        <path d="M12,6l-7,7v8h5v-4c0-1.1,0.9-2,2-2s2,0.9,2,2v4h5v-8L12,6z"></path>
                                    </g>
                                    </g></svg>
                            </div>
                            <input type="text" name="direccion" class="form-control" id="inputDireccion" maxlength="100" placeholder="$direccion" value="$vDireccion" required>
                        </div>
                        <!--FIN CAMPO DNI DE FORMULARIO-->					
                        <!--INICIO CAMPO CONTRASEÑA DE FORMULARIO-->
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g fill='#FFFFFF'><path d="M21 10H10.6C9.8 8.2 8 7 6 7c-5 0-5 4-5 5s0 5 5 5c2.2 0 4-1.4 4.7-3.3 0 0 .3-.7 1.3-.7s1 1 1 1 0 1 1 1 1.1-1 1.1-1-.1-1 .9-1 1 1 1 1 0 1 1 1 1-1 1-1 0-1 1-1 1 1 1 1 0 1 1 1 1-1 1-1v-2c0-1.1-.9-2-2-2zM4.1 14c-1 0-1.1-.9-1.1-2s.1-2 1.1-2 1.9.9 1.9 2-.8 2-1.9 2z"></path></g></svg>
                                </span>
                            </div>
                            <input type="password" name="password" class="form-control" id="inputPassword" placeholder="$password" required>
                        </div>
                        <!--FIN CAMPO CONTRASEÑA DE FORMULARIO-->					
                        
                        <div class="form-group" id="divBotones">
                            <input type="submit" value="$boton" class="btn float-right login_btn" id="submit">
                        </div>
                        <input type="hidden" name="tipoUsuario" value="$tipoUsuario">
                        <input type="hidden" name="accion" value="$tarea" id="inputAccion">
                    </form>
                </div>
            </div>
        </div>
    </div>
    FORM;
    echo $form;
}