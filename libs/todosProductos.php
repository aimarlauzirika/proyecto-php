<?php

include_once "db.php";

$sql = "SELECT producto.codProducto, nombre, tipo, stock, precio, prodMin, img, sum(unidades) vendidos from producto
left join contenidopedido on producto.codProducto = contenidopedido.codProducto
WHERE tipo != 'be'  AND tipo != 'ex'
group by nombre
order by vendidos desc";

$resp = $conn->query($sql);
$productos = $resp->fetch_all(MYSQLI_ASSOC);

// print_r(json_encode($productos));
?>