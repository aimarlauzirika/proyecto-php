<?php

include_once 'db.php';

$numDiaSemana = date("w");
// echo "Día de la semana: ". $numDiaSemana . "\n";
$primerDia = date("Y-m-d", mktime(1,1,1, 4, date("d")-7-$numDiaSemana+1, 2022));
// echo $primerDia;
$ultimoDia = date("Y-m-d", mktime(1,1,1, 4, date("d")-$numDiaSemana, 2022));
// echo $ultimoDia;

$sql = "SELECT fecha, servicio, sum(numComensales) numComensales FROM reserva 
WHERE fecha >= '$primerDia' AND fecha <= '$ultimoDia'
GROUP BY fecha, servicio
ORDER BY fecha, servicio";
// echo json_encode($sql);
// exit;

$result = $conn->query($sql);
$reservasSemana = $result->fetch_all(MYSQLI_ASSOC);
print_r(json_encode($reservasSemana));

?>