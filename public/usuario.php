<?php
include_once "../libs/const.php";
include_once '../libs/funciones.php';
include_once '../libs/db.php';

if (isset($_SESSION['nombre'])) {
    $nombre = $_SESSION['nombre'];
    $apellido = $_SESSION['apellido'];
    $email = $_SESSION['email'];
    $direccion = $_SESSION['direccion'];
    $dni = $_SESSION['dni'];
    $tipoUsuario = $_SESSION['tipoUsuario'];
} else {
    header('Location: login.php');
}


include_once HEADER_DIR;
?>

<div class="main">
    <div class="menu-usuario">
        <p class="link-usuario selec" id="navPerfil">Mi Perfil</p>
        <p class="link-usuario" id="navPedidos">Mis Pedidos</p>
        <p class="link-usuario" id="navReservas">Mis Reservas</p>
        <a class="link-usuario" href="../libs/logout.php">Cerrar Sesión</a>
    </div>

    <div class="" id="areaPerfil">
        <h2 class="h2-usuario">Tus Datos</h2>
        <div class="perfil">
            <p><strong>Nombre: </strong><?= $nombre ?></p>
            <p><strong>Apellido: </strong><?= $apellido ?></p>
            <p><strong>Email: </strong><?= $email ?></p>
            <p><strong>Dirección: </strong><?= $direccion ?></p>
            <p><strong>DNI: </strong><?= $dni ?></p>
            <button class="btn-usuario editar" id="btnEditarPerfil">Editar</button>
            <div class="form-editar-usuario ocultar">
                <?php
                $datos['nombre'] = $nombre;
                $datos['apellido'] = $apellido;
                $datos['email'] = $email;
                $datos['direccion'] = $direccion;
                $datos['dni'] = $dni;
                formUsuario($tipoUsuario, 'editar', 'Edita tus Datos', $datos);
                ?>
            </div>
        </div>
    </div>
    
    <div class="ocultar" id="areaPedidos">
        <?php
        $sql = "SELECT pedido.numPedido, fecha, hora, pedido.tipo, precio, SUM(precio) total FROM pedido
        INNER JOIN contenidopedido ON pedido.numPedido = contenidopedido.numPedido
        INNER JOIN producto ON contenidopedido.codProducto = producto.codProducto
        WHERE dniUsuario = '$dni'
        GROUP BY numPedido
        ORDER BY fecha DESC";
        
        $result = $conn->query($sql);
        
        $pedidos = $result->fetch_all(MYSQLI_ASSOC);
        // debuggear($pedidos);
        if ($result->num_rows == 0) :
            echo '<h2 class="h2-usuario">No has realizado Pedidos</h2><br><br><br><br><br><br><br>';
        else:?>
            <table class="tabla-pedidos-usuario">
            <thead>
                <tr><td>Núm. Pedido</td><td>Fecha</td>
                <td>Hora</td>
                <td>Tipo</td><td>Precio</td></tr>
            </thead>
            <tbody>
                <?php foreach($pedidos as $pedido): ?>
                    <?php $numPedido = $pedido['numPedido'] ?>
                    <tr class="trPedido" id="<?=$numPedido?>">
                        <td><?=$pedido['numPedido']?></td>
                        <td><?=$pedido['fecha']?></td>
                        <td><?=$pedido['hora']?></td>
                        <td><?=$pedido['tipo']==1?'Recoger':'A domicilio'?></td>
                        <td><?=number_format($pedido['total'],2,',')?> €</td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <?php endif ?>
        
        

        <div id="verPedido">
        </div>
    </div>

    <div class="ocultar" id="areaReservas">
        <?php
        $sql = "SELECT * FROM reserva WHERE email = '$email' ORDER BY fecha DESC";
        // echo $sql;
        $result = $conn->query($sql);
        // debuggear($result);
        $fechaHoy = date('Y-m-d');

        if ($result->num_rows == 0) {
            echo '<h2 class="h2-usuario">No tienes Reservas</h2><br><br><br><br><br><br><br>';
        } else {
            echo '<h2 class="h2-usuario">TUS RESERVAS</h2><br>';
        }
        ?>
        <div class="contenedor-reservas">
            <div class="lista-reservas">
                <?php while ($row = $result->fetch_assoc()) : ?>
                    <!-- print_r($row); -->
                    <?php $anterior = $fechaHoy > $row['fecha']; ?>
                    <?php $numReserva = $row['numReserva']; ?>
                    <div class="div-reserva<?= $anterior ? ' anterior' : '' ?>" id="<?= $numReserva ?>">
                        <p><strong>Fecha:</strong> <?= $row['fecha'] ?></p>
                        <p><strong>Servicio:</strong> <?= $row['servicio'] == 0 ? 'Mediodía' : 'Cena' ?></p>
                        <p><strong>Número de Comensales:</strong> <?= $row['numComensales'] ?></p>
                        <?php if (!$anterior) : ?>
                            <!-- <button class="btn-reserva editar">Editar</button> -->
                            <button class="btn-usuario anular">Anular</button>
                        <?php endif ?>
                    </div>
                <?php endwhile ?>
            </div>

        </div>
    </div>

</div>









<?php $conn->close() ?>
<?php include_once FOOTER_DIR ?>
<script src="../public/js/usuario.js"></script>