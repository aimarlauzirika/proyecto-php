inicio();
async function inicio() {
    const [reservasSemana, ingresosSemana, vendidos, numPedidos] = await Promise.all([
        obtenerReservasSemana(),
        obtenerIngresosSemana(), 
        obtenerVendidos(),
        obtenerNumPedidos(),
    ])
    // console.log(reservasSemana)

    const diaSemana = new Date().getDay()
    // consoles.log(diaSemana)
    const semanaPasada = new Date(new Date().getTime() - (24*60*60*1000)*7)
    // consoles.log(semanaPasada)
    const lunesSemanaPasada = new Date(new Date().getTime() - (24*60*60*1000)*(7+diaSemana-1))
    // consoles.log(lunesSemanaPasada)

    graficoComensales(reservasSemana, diaSemana, numPedidos);
    graficoIngresos(ingresosSemana, diaSemana);
    graficoVendidos(vendidos)
}

// Obtener datos de la base datos 
async function obtenerReservasSemana() {
    const resp = await fetch('../libs/obtenerReservasSemana.php');
    const reservasSemana = await resp.json();
    return reservasSemana;
}
async function obtenerIngresosSemana() {
    const resp = await fetch('../libs/obtenerIngresosSemana.php');
    const ingresosSemana = await resp.json();
    // console.log(ingresosSemana);
    return ingresosSemana;
}
async function obtenerVendidos() {
    const response = await fetch('../libs/obtenerProductos.php')
    const data = await response.json()
    return data
}
async function obtenerNumPedidos() {
    const response = await fetch('../libs/obtenerNumPedidos.php')
    const data = await response.json()
    // console.log(data);
    return data
}

function graficoComensales(reservasSemana, diaSemana, numPedidos) {
    // console.log(reservasSemana);

    const fill = (number, len) => "0".repeat(len - number.toString().length) + number.toString();
    let datos = {
        1:{0:0, 1:0, 'pedidos':0},
        2:{0:0, 1:0, 'pedidos':0},
        3:{0:0, 1:0, 'pedidos':0},
        4:{0:0, 1:0, 'pedidos':0},
        5:{0:0, 1:0, 'pedidos':0},
        6:{0:0, 1:0, 'pedidos':0},
        7:{0:0, 1:0, 'pedidos':0}
    };
    for (let i = 0; i < 7; i++) {
        const fecha = new Date(new Date().getTime() - (24*60*60*1000)*(7+diaSemana-1-i))


        const dia = fill(fecha.getDate(), 2)
        const mes = fill(fecha.getMonth()+1, 2)
        const year = fecha.getFullYear()
        const fechaString = `${year}-${mes}-${dia}`
        // console.log(fechaString);
        for (let j = 1; j <= reservasSemana.length; j++) {
            if (fechaString == reservasSemana[j-1].fecha) {
                if (reservasSemana[j-1].servicio == "0") {
                    datos[i+1][0] = reservasSemana[j-1].numComensales;
                } else if (reservasSemana[j-1].servicio == "1") {
                    datos[i+1][1] = reservasSemana[j-1].numComensales;
                }
            }
        }
        for (let j = 1; j <= numPedidos.length; j++) {
            if (fechaString == numPedidos[j-1].fecha) {
                datos[i+1].pedidos = numPedidos[j-1].numPedidos;
            }
        }
        
    }
    // console.log(datos);

    const ctxComensalesDiaSemana = document.getElementById("canvasComensalesDiaSemana").getContext("2d")
    const chartComensalesDiaSemana = new Chart(ctxComensalesDiaSemana, {
        type: 'bar',
        data: {
            labels: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
            datasets: [{
                label: 'Comida',
                data: [
                parseInt(datos[1][0]),
                parseInt(datos[2][0]),
                parseInt(datos[3][0]),
                parseInt(datos[4][0]),
                parseInt(datos[5][0]),
                parseInt(datos[6][0]),
                parseInt(datos[7][0]),
                ],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                ],
                borderWidth: 1
            },
            {
                label: 'Cena',
                data: [
                parseInt(datos[1][1]),
                parseInt(datos[2][1]),
                parseInt(datos[3][1]),
                parseInt(datos[4][1]),
                parseInt(datos[5][1]),
                parseInt(datos[6][1]),
                parseInt(datos[7][1]),
                ],
                backgroundColor: [
                    'rgba(133, 5, 100, 0.2)',
                ],
                borderColor: [
                    'rgba(133, 5, 100, 1)',
                ],
                borderWidth: 1
            },{
                label: 'Pedidos',
                data: [
                parseInt(datos[1].pedidos),
                parseInt(datos[2].pedidos),
                parseInt(datos[3].pedidos),
                parseInt(datos[4].pedidos),
                parseInt(datos[5].pedidos),
                parseInt(datos[6].pedidos),
                parseInt(datos[7].pedidos),
                ],
                backgroundColor: [
                    'rgba(248, 215, 29, 0.2)',
                ],
                borderColor: [
                    'rgb(248, 215, 29)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Comensales (20 máx.) y pedidos última semana'
                }
            },
            scales: {
                y: {
                    // min: 0,
                    // max: 20,
                }
            }
        }
    })

}

function graficoIngresos(ingresosSemana, diaSemana) {

    const fill = (number, len) => "0".repeat(len - number.toString().length) + number.toString();
    let datos = [0, 0, 0, 0, 0, 0, 0];
    for (let i = 0; i < 7; i++) {
        const fecha = new Date(new Date().getTime() - (24*60*60*1000)*(7+diaSemana-1-i))
        // console.log(fecha);

        const dia = fill(fecha.getDate(), 2)
        const mes = fill(fecha.getMonth()+1, 2)
        const year = fecha.getFullYear()
        const fechaString = `${year}-${mes}-${dia}`
        // console.log(fechaString);

        for (let j = 1; j <= ingresosSemana.length; j++) {
            if (fechaString == ingresosSemana[j-1].fecha) {
                datos[i] = parseFloat(ingresosSemana[j-1].ingresos);
            }
        }
    }
    // console.log(datos);
    //* Gráfico Ingresos y Gastos
    const ctxIngresosGastos = document.getElementById("canvasIngresosGastos").getContext("2d")
    const chartIngresosGastos = new Chart(ctxIngresosGastos, {
        type: 'line',
        data: {
            labels: [`Lunes ${ingresosSemana[0].numDiaMes}`, 
            `Martes ${ingresosSemana[1].numDiaMes}`, 
            `Miércoles ${ingresosSemana[2].numDiaMes}`, 
            `Jueves ${ingresosSemana[3].numDiaMes}`, 
            `Viernes ${ingresosSemana[4].numDiaMes}`, 
            `Sábado ${ingresosSemana[5].numDiaMes}`, 
            `Domingo ${ingresosSemana[6].numDiaMes}`],
            datasets: [{
                label: 'Ingresos',
                data: datos,
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            },
            {
                label: 'Gastos',
                data: [datos[0]/4+40, datos[1]/4+40, datos[2]/4+40, datos[3]/4+40, datos[4]/4+40, datos[5]/4+40, datos[6]/4+40],
                fill: false,
                borderColor: 'rgb(225, 192, 192)',
                tension: 0.1
            }]
        }
    })
}

function graficoVendidos(vendidos) {
    // console.log(vendidos);
    
    const ensaladas = vendidos.filter(plato=>plato['tipo']== "en")
    const pastas = vendidos.filter(plato=>plato['tipo']== "pa")
    const carnes = vendidos.filter(plato=>plato['tipo']== "ca")
    const pizzas = vendidos.filter(plato=>plato['tipo']== "pi")
    // console.log(ensaladas);
    const legendPosition = "bottom"
    
    const ctxEnsaladasVendidas = document.getElementById("canvasEnsaladasMasVendidas").getContext("2d")
    const chartEnsaladasVendidas = new Chart(ctxEnsaladasVendidas, {
        type: 'doughnut',
        data: {
            labels: [ensaladas[0].nombre, ensaladas[1].nombre, ensaladas[2].nombre, ensaladas[3].nombre],
            datasets: [{
                label: 'Comensales última semana',
                data: [ensaladas[0].vendidos, ensaladas[1].vendidos, ensaladas[2].vendidos, ensaladas[3].vendidos],
                backgroundColor: [
                    '#FFC312',
                    '#A8133C',
                    '#753043',
                    '#A89E13'
                    
                ],
                hoverOffset: 1
            }]
        },
        options: {
            plugins: {
                legend: {
                    position: legendPosition
                }
            }
        }
    })
    //* Gráfico pizzas más vendidas
    const ctxPizzasVendidas = document.getElementById("canvasPizzasMasVendidas").getContext("2d")
    const chartPizzasVendidas = new Chart(ctxPizzasVendidas, {
        type: 'doughnut',
        data: {
            labels: [pizzas[0].nombre, pizzas[1].nombre, pizzas[2].nombre, pizzas[3].nombre],
            datasets: [{
                label: 'Comensales última semana',
                data: [pizzas[0].vendidos, pizzas[1].vendidos, pizzas[2].vendidos, pizzas[3].vendidos],
                backgroundColor: [
                    '#1358A8',
                    '#02D9DB',
                    '#E0603A)',
                    '#A81204'
                ],
                hoverOffset: 1
            }]
        },
        options: {
            plugins: {
                legend: {
                    position: legendPosition
                }
            }
        }
    })
    //* Gráfico pastas más vendidas
    const ctxPastasVendidas = document.getElementById("canvasPastasMasVendidas").getContext("2d")
    const chartPastasVendidas = new Chart(ctxPastasVendidas, {
        type: 'doughnut',
        data: {
            labels: [pastas[0].nombre, pastas[1].nombre, pastas[2].nombre, pastas[3].nombre],
            datasets: [{
                label: 'Comensales última semana',
                data: [pastas[0].vendidos, pastas[1].vendidos, pastas[2].vendidos, pastas[3].vendidos],
                backgroundColor: [
                    '#03A892',
                    '#1ADB54',
                    '#E05395',
                    '#A816A4'
                ],
                hoverOffset: 1
            }]
        },
        options: {
            plugins: {
                legend: {
                    position: legendPosition
                }
            }
        }
    })
    //* Gráfico carnes más vendidas
    const ctxCarnesVendidas = document.getElementById("canvasCarnesMasVendidas").getContext("2d")
    const chartCarnesVendidas = new Chart(ctxCarnesVendidas, {
        type: 'doughnut',
        data: {
            labels: [carnes[0].nombre, carnes[1].nombre, carnes[2].nombre, carnes[3].nombre],
            datasets: [{
                label: 'Comensales última semana',
                data: [carnes[0].vendidos, carnes[1].vendidos, carnes[2].vendidos, carnes[3].vendidos],
                backgroundColor: [
                    '#47A803',
                    '#DBD51A',
                    '#6653E0',
                    '#031AA8'
                ],
                hoverOffset: 1
            }]
        },
        options: {
            plugins: {
                legend: {
                    position: legendPosition
                }
            }
        }
    })
}


//* Eliminar empleado

const botonesEliminar = document.querySelectorAll('.eliminar-empleado');
botonesEliminar.forEach(boton => {
    boton.addEventListener('click', async(e)=>{
        // console.log(e.target.parentElement.parentElement.childNodes[1].textContent);
        const nombre = e.target.parentElement.parentElement.firstChild.textContent
        const apellido = e.target.parentElement.parentElement.childNodes[1].textContent
        const confirmar = confirm(`¿Seguro que quieres eliminar a ${nombre} ${apellido} de la base de datos?`)
        if (confirmar) {
            const dni = e.target.parentElement.parentElement.id
            const divEmpleado = e.target.parentElement.parentElement
    
            const Formd = new FormData;
            Formd.append('dni', dni)
            const url = '../libs/eliminarEmpleado.php'
            fetch(url, {
                body: Formd,
                method: 'POST'
            })
            // .then(res => res.json())
            // .then(response => console.log('Success:', response))
            // const data = await res.json();
            // console.log(data);
            .then(divEmpleado.remove())
        }
    })
});


//* Editar empleado
$inputNombre = document.querySelector('#inputNombre') 
$inputApellido = document.querySelector('#inputApellido') 
$inputDni = document.querySelector('#inputDni') 
$inputEmail = document.querySelector('#inputEmail') 
$inputDireccion = document.querySelector('#inputDireccion') 
$inputPassword = document.querySelector('#inputPassword')

$inputNombre.value = "";
$inputApellido.value = "";
$inputDni.value = "";
$inputEmail.value = "";
$inputDireccion.value = "";
$inputPassword.value = "";

const botonesEditar = document.querySelectorAll('.editar-empleado');
botonesEditar.forEach(boton => {
    boton.addEventListener('click', async(e)=>{

        //* Obtener info del empleado de la bd
        const dni = e.target.parentElement.parentElement.id
        // console.log(dni)
        const Formd = new FormData;
        Formd.append('dni', dni)
        const result = await fetch('../libs/obtenerUnUsuario.php', {
            method: 'POST',
            body: Formd
        })
        const usuario = await result.json();
        // console.log(usuario);
        
        //* Editar el título del formulario
        document.querySelector('#tituloForm').textContent = `Editar información de ${usuario['nombre']} ${usuario['apellido']}`;
        
        //* Rellenar el formulario con la info del usuario

        $inputNombre.value = usuario['nombre'];
        $inputApellido.value = usuario['apellido'];
        $inputDni.value = usuario['dni'];
        $inputEmail.value = usuario['email'];
        $inputDireccion.value = usuario['direccion'];

        //* Modificar el botón
        const submit = document.querySelector('#submit');
        submit.value = "Actualizar";

        //* Modificar el tipoUsuario
        const inputAccion = document.querySelector('#inputAccion')
        inputAccion.value = "editar"

        //* Añadir dniOriginal

        // Eliminar dniOriginal previo
        const dniOriginalPrevio = document.querySelector('input[name="dniOriginal"]')
        if (dniOriginalPrevio) {
            dniOriginalPrevio.remove()
        }

        const formUsuario = document.querySelector('#formUsuario')
        const hiddenDni = document.createElement('INPUT')
        hiddenDni.type = "hidden"
        hiddenDni.value = usuario['dni']
        hiddenDni.name = "dniOriginal"
        formUsuario.insertBefore(hiddenDni, inputAccion)



        //* Añadir boton cancelar
        const previo = document.querySelector('#btnCancelar')
        if (previo) { previo.remove() }
        const btnCancelar = document.createElement('DIV')
        btnCancelar.textContent = "Cancelar"
        btnCancelar.id = "btnCancelar"
        btnCancelar.classList.add('cancelar-edicion', 'btn', 'float-left')
        const divBotones = document.querySelector('#divBotones')
        divBotones.insertBefore(btnCancelar, submit)

        btnCancelar.addEventListener('click', ()=>{

            document.querySelector('#tituloForm').textContent = 'Añadir Empleado'

            $inputNombre.value = "";
            $inputApellido.value = "";
            $inputDni.value = "";
            $inputEmail.value = "";
            $inputDireccion.value = "";
            $inputPassword.value = "";

            submit.value = "Registrar";
            btnCancelar.remove()
            inputAccion.value = "nuevo"
        })
    })
})