
const inputs = document.querySelectorAll('.input-num-platos');
const selectRecoger = document.querySelector('#recoger');
const tbody = document.querySelector('#tbody');

obtenerResumen();


selectRecoger.addEventListener('change', obtenerResumen);

inputs.forEach(input => {
    input.addEventListener('change', obtenerResumen);
});


// * * mostrar y ocultar sección * * \\

let seccionElegida;
//* click en el navegador de secciones
const pedidoNav = document.querySelector('#pedidoNav'); 
pedidoNav.addEventListener('click', (e)=>{
    // console.log(e.target.textContent);
    seccionElegida = e.target.textContent;
    const element = e.target;
    // console.log(element.classList);
    if (!element.classList.contains('active')) {
        const active = document.querySelector('.pedido-nav-option.active')
        active.classList.remove('active')
        element.classList.add('active')
    }
    //* Cambiar mostrar tabla seleccionada
    const secciones = document.querySelectorAll('.seccion')
    secciones.forEach(seccion=>{
        // console.log(seccion);
        if(!seccion.classList.contains('ocultar')){
            seccion.classList.add('ocultar')
        }
        // console.log(seccion.classList);
        if(seccion.classList.contains(seccionElegida)){
            console.log(`La sección elegida es ${seccionElegida}`);
            seccion.classList.remove('ocultar');
        }
    })
})

        
        
function obtenerResumen() {
    const tdTotal = document.querySelector('#total'); // elemento del html total
    var total = 0; // precio total (suma total)
    
    // eliminar contenido anterior
    tbody.textContent = '';
    tdTotal.textContent = 'Total: 0,00€'
            
    // añadir contenido
    inputs.forEach(input => {
        if (input.value != 0) {
            const tabla = input.parentElement.parentElement.parentElement.parentElement;
            tabla.classList.remove('hidden');
            const plato = input.parentElement.parentElement.firstElementChild.textContent;
            const precio = input.parentElement.parentElement.dataset.precio;
            const cantidad = input.value;
            
            total += precio * cantidad;
            
            
            const tr = document.createElement('TR');
            const tdPlato = document.createElement('TD');
            tdPlato.textContent = plato;
            tr.appendChild(tdPlato);
            
            const tdPrecio = document.createElement('TD');
            tdPrecio.textContent = precio + '€';
            tr.appendChild(tdPrecio);
            
            const tdCantidad = document.createElement('TD');
            tdCantidad.textContent = cantidad;
            tr.appendChild(tdCantidad);
            
            tbody.appendChild(tr);
            
            const tdTotal = document.querySelector('#total');
            tdTotal.textContent = 'Total: ' + total.toFixed(2) + '€'; // .toFixed para determinar el número de decimales
            
        }
    });

    // añadir modo de envío a la tabla resumen
    if (selectRecoger.value == 'domicilio') { // cuando se envía a domicilio se suman 2€
        const tr = document.createElement('TR');
        const tdRecoger = document.createElement('TD');
        tdRecoger.textContent = 'Envío a domicilio';
        tr.appendChild(tdRecoger);
        const tdPrecio = document.createElement('TD');
        tdPrecio.textContent = '2€';
        tr.appendChild(tdPrecio);
        const tdCantidad = document.createElement('TD');
        tdCantidad.textContent = '';
        tr.appendChild(tdCantidad);
        tbody.appendChild(tr);
        total += 2;
        
        // tdTotal.textContent = 'Total: ' + total + '€';
    } else if (selectRecoger.value == 'recoger') { // cuando se recoge en restaurante se suma 0€
        
        const tr = document.createElement('TR');
        const tdRecoger = document.createElement('TD');
        tdRecoger.textContent = 'Recoger en restaurante';
        tr.appendChild(tdRecoger);
        const tdPrecio = document.createElement('TD');
        tdPrecio.textContent = '0€';
        tr.appendChild(tdPrecio);
        const tdCantidad = document.createElement('TD');
        tdCantidad.textContent = '';
        tr.appendChild(tdCantidad);
        tbody.appendChild(tr);
        
        // tdTotal.textContent = 'Total: ' + total + '€';
    }
    total = new Intl.NumberFormat('es', {style: 'currency',currency: 'EUR', minimumFractionDigits: 2}).format(total);
    tdTotal.textContent = 'Total: ' + total;
    validarFormulario();
}


function validarFormulario() {
    
    
    //* Validar a domicilio
    if (selectRecoger.value == '') {
        selectRecoger.setCustomValidity("No has elegido el modo de envío");
    } else {
        selectRecoger.setCustomValidity('');
    }
    
    //* Validar platos
    if (tbody.childElementCount < 2) {
        const btnSubmit = document.querySelector('#btnSubmit');
        btnSubmit.setCustomValidity("No has elegido ningún plato");
    } else {
        btnSubmit.setCustomValidity('');
    }
}

//* Por alguna razón se oculta el select
setTimeout(() => {
    selectRecoger.style.display = "";
    const div = document.querySelector('div[tabindex="0"]');
    // console.log(div);
    div.style.display = "none";
}, 500);
