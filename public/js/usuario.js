const areaPerfil = document.querySelector('#areaPerfil')
const areaPedidos = document.querySelector('#areaPedidos')
const areaReservas = document.querySelector('#areaReservas')
const navPerfil = document.querySelector('#navPerfil')
const navPedidos = document.querySelector('#navPedidos')
const navReservas = document.querySelector('#navReservas')
//* * PERFIL * *\\
//
navPerfil.addEventListener('click', ()=>{
    areaPerfil.classList.remove('ocultar')
    navPerfil.classList.add('selec')
    areaReservas.classList.add('ocultar')
    navReservas.classList.remove('selec')
    areaPedidos.classList.add('ocultar')
    navPedidos.classList.remove('selec')
})

const btnEditarPerfil = document.querySelector('#btnEditarPerfil')
btnEditarPerfil.addEventListener('click', ()=> {
    const form = document.querySelector('.form-editar-usuario')
    form.classList.remove('ocultar')
})
//
//* * FIN PERFIL * *\\



//* * PEDIDOS * *\\
//
navPedidos.addEventListener('click', ()=>{
    areaPedidos.classList.remove('ocultar')
    navPedidos.classList.add('selec')
    areaPerfil.classList.add('ocultar')
    navPerfil.classList.remove('selec')
    areaReservas.classList.add('ocultar')
    navReservas.classList.remove('selec')
})


const trPedidos = document.querySelectorAll('.trPedido')
const verPedido = document.querySelector('#verPedido')
trPedidos.forEach((tr)=>{
    tr.addEventListener('click', async e=>{
        //* eliminar tabla (si hay)
        const tablaAnterior = document.querySelector('#tablaPedido')
        if (tablaAnterior) {
            tablaAnterior.remove()
        }
        //* quitar clase ver
        trPedidos.forEach(lineaPedido => {
            lineaPedido.classList.remove('ver')
        });
        //* añadir clase ver
        const linea = e.target.parentElement
        linea.classList.add('ver')
        //* Obtener infoPedido
        const numPedido = linea.id
        // console.log(id);
        const peticion = await fetch(`../libs/obtenerPedidoUsuario.php?numPedido=${numPedido}`)
        const infoPedido = await peticion.json()
        console.log(infoPedido);
        
        //* Crear la tabla
        const tablaPedido = document.createElement('TABLE')
        tablaPedido.id = 'tablaPedido'

        const thead = document.createElement('THEAD')

        const thNombre = document.createElement('TH')
        thNombre.textContent = 'Nombre'
        thead.appendChild(thNombre)

        const thPrecio = document.createElement('TH')
        thPrecio.textContent = 'Precio'
        thead.appendChild(thPrecio)

        const thUnidades = document.createElement('TH')
        thUnidades.textContent = 'Unidades'
        thead.appendChild(thUnidades)

        const thResultado = document.createElement('TH')
        thResultado.textContent = 'Resultado'
        thead.appendChild(thResultado)

        tablaPedido.appendChild(thead)
        infoPedido.forEach(producto => {

            const trPlato = document.createElement('TR')

            const tdNombre = document.createElement('TD')
            tdNombre.textContent = producto.nombre
            trPlato.appendChild(tdNombre)
            
            const tdPrecio = document.createElement('TD')
            tdPrecio.textContent = parseFloat(producto.precio).toLocaleString('es', {style: 'currency',currency: 'EUR', minimumFractionDigits: 2})
            trPlato.appendChild(tdPrecio)
            
            const tdUnidades = document.createElement('TD')
            tdUnidades.textContent = producto.unidades
            trPlato.appendChild(tdUnidades)
                  
            const tdResultado = document.createElement('TD')
            tdResultado.textContent = parseFloat(producto.resultado).toLocaleString('es', {style: 'currency',currency: 'EUR', minimumFractionDigits: 2})
            trPlato.appendChild(tdResultado)
            
            tablaPedido.appendChild(trPlato)
            verPedido.appendChild(tablaPedido)
        });

        // console.log(document.querySelector('.trPedido.ver').lastElementChild.previousElementSibling.textContent);
        const tipo = document.querySelector('.trPedido.ver').lastElementChild.previousElementSibling.textContent
        if(tipo == 'A domicilio'){
            const trDomicilio = document.createElement('TR')
            
            const tdNombre = document.createElement('TD')
            tdNombre.textContent = 'Envío a Domicilio'
            trDomicilio.appendChild(tdNombre)
            
            const tdPrecio = document.createElement('TD')
            tdPrecio.textContent = '2,00 €'
            trDomicilio.appendChild(tdPrecio)
            
            const tdUnidades = document.createElement('TD')
            tdUnidades.textContent = 1
            trDomicilio.appendChild(tdUnidades)
                  
            const tdResultado = document.createElement('TD')
            tdResultado.textContent = '2,00 €'
            trDomicilio.appendChild(tdResultado)
            
            tablaPedido.appendChild(trDomicilio)
            verPedido.appendChild(tablaPedido)
        }


        const tfoot = document.createElement('TFOOT')
        tfoot.classList.add('tfood')

        const th1 = document.createElement('TH')
        tfoot.appendChild(th1)
        const th2 = document.createElement('TH')
        tfoot.appendChild(th2)
        const thTotal = document.createElement('TH')
        thTotal.textContent = 'Total'
        tfoot.appendChild(thTotal)
        const tdTotalResultado = document.createElement('TD')
        const total = document.querySelector('.trPedido.ver').lastElementChild.textContent
        tdTotalResultado.textContent = total
        tfoot.appendChild(tdTotalResultado)

        tablaPedido.appendChild(tfoot)
    })
})
//
//* * FIN PEDIDOS * *\\



//* * RESERVAS  * *\\
//
navReservas.addEventListener('click', ()=>{
    areaReservas.classList.remove('ocultar')
    navReservas.classList.add('selec')
    areaPerfil.classList.add('ocultar')
    navPerfil.classList.remove('selec')
    areaPedidos.classList.add('ocultar')
    navPedidos.classList.remove('selec')
})

const btnAnular = document.querySelectorAll('.anular')
// console.log(btnAnular);

btnAnular.forEach(btn => {
    btn.addEventListener('click', (e)=>{
        numReserva = e.target.parentElement.id
        // console.log(numReserva);
        fetch(`../libs/eliminarReserva.php?numReserva=${numReserva}`)
        .then(()=>location.reload())
    })    
});
//
//* * FIN RESERVAS  * *\\