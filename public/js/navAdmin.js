
//* Clase active al nav_link y Mostrar seccion
const navList = document.querySelector('#navList')
const navLinks = document.querySelectorAll('.nav_link.seccion')
navList.addEventListener('click', (e) => {
    const element = e.target
    const tagName = e.target.tagName
    if (tagName == 'P' || tagName == 'I' || tagName == 'SPAN') {
        navLinks.forEach(navLink => {
            navLink.classList.remove('active')
        })
        let id;
        let selectorId;
        let p;
        if (tagName == 'P') {
            p = element
            id = element.id
        } else {
            p = element.parentElement
            id = element.parentElement.id
        }
        p.classList.add('active')
        
        // Obtener id sección
        const mayus = id[0].toUpperCase() + id.slice(1);
        selectorId = '#contenedor'+mayus
        mostrarSeccion(selectorId)

        ocultarMenu()
    }
})

//* Mostrar menú
const arrowLink = document.querySelector('#arrowLink')
const nav = document.querySelector('#nav-bar')
const arrow = document.querySelector('#arrow')
arrowLink.addEventListener('click', ()=>{
    nav.classList.toggle('show')
    arrow.classList.toggle('bx-right-arrow-alt')
})

//* Función Ocultar menú
function ocultarMenu() {
    nav.classList.remove('show')
    arrow.classList.add('bx-right-arrow-alt')
}

//* Función Mostrar sección
const secciones = document.querySelectorAll('.contenedor_admin') 
function mostrarSeccion(id) {
    secciones.forEach(seccion =>{
        if(!seccion.classList.contains('ocultar')){
            seccion.classList.add('ocultar')
        }
        document.querySelector(id).classList.remove('ocultar')
    })
}