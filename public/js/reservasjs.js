setTimeout(() => {
    inputServicio.style = "display: 'block'"
    inputServicio.nextSibling.style = 'display: none'
    inputNumComensales.style = "display: 'block'"
    inputNumComensales.nextSibling.style = 'display: none'

}, 100)

const inputFecha = document.querySelector("#fecha")
const inputServicio = document.querySelector("#servicio")
const inputNumComensales = document.querySelector("#numComensales")

// console.log(inputServicio.value)

inputFecha.addEventListener("change", actualizarDisponibilidad)
inputServicio.addEventListener("change", actualizarDisponibilidad)

async function actualizarDisponibilidad() {
    const fecha = inputFecha.value
    const servicio = inputServicio.value
    const plazasReservadas = await reservaDynamic(fecha, servicio)
    const plazasDisponibles = 20 - plazasReservadas
    console.log(plazasDisponibles)

    while (inputNumComensales.childElementCount) {
        if (inputNumComensales.childElementCount) {
            inputNumComensales.removeChild(inputNumComensales.childNodes[0])
        }
    }
    const option = document.createElement("option")
    option.value = ""
    option.textContent = "---"
    option.disabled = "true"
    option.selected = "true"
    inputNumComensales.appendChild(option)
    for (i = 1; i <= plazasDisponibles; i++) {
        const option = document.createElement("option")
        option.value = i
        option.textContent = i
        inputNumComensales.appendChild(option)
    }

}



async function reservaDynamic(fecha, servicio) {
    const varFetch = await fetch("../libs/reservasDinamicas.php?fecha=" + fecha + `&servicio=${servicio}`)
        // console.log(varFetch)
    let resultFetch = await varFetch.json()
    if (resultFetch == null) {
        resultFetch = 0
    }
    // console.log(resultFetch)
    return resultFetch
}