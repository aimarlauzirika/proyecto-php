//* Cambiar color página seleccionada

const array = window.location.href.split("public/")
const pagina = array[1];

const nav = document.querySelectorAll(".nav-item")
// console.log(nav);
nav.forEach(element => {
    element.classList.remove('active')
});
const selector = 'li > [href="'+pagina+'"]'
// console.log(selector);
const element = document.querySelector(selector)
// console.log(element);
if (element) {
    element.parentElement.classList.add('active')
}