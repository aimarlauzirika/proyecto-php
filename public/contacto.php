<?php include_once "../libs/const.php"; ?>        
<?php include_once "../libs/funciones.php"; ?> 
<?php include_once HEADER_DIR; ?> 

  <!-- book section -->
  <div class="horario">
    <img class= "apertura"src="images/horario2.jpg">
  </div>
  <section class="book_section layout_padding">
    <div class="contacto">
        <h2>
          CONTACTA CON NOSOTROS
        </h2>
      <div class="datos-mapa">
      
          <div class="form_container">
            <div class="footer_contact">
              <div class="contact_link_box">
                
                <p>Si tienes cualquier consulta sobre nuestro servicio, así como si quieres hacernos llegar algún tipo de sugerencia, puedes ponerte en contacto con nosotros:</p>
                <a href="mailto:puntoitalia@gmail.com">puntoitalia@gmail.com
                <i class="fa fa-envelope" aria-hidden="true"></i></br></a>

                <a href="tel:+34655861184">Llámanos +34 655861184
                <i class="fa fa-phone" aria-hidden="true"></i></br></a>

                
              </div>
            </div>
          </div>
        
          <div class="map_container ">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2814.8984471463377!2d-2.9045955845175038!3d43.25767127913681!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4e4fac87437727%3A0x364f27e82def0130!2sCIFP%20Txurdinaga%20LHII!5e1!3m2!1ses!2ses!4v1650807230267!5m2!1ses!2ses" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end book section -->

  <!-- footer section -->
  <?php include_once FOOTER_DIR; ?> 
<!--Fin de Footer--> 
