<!DOCTYPE html>
    <html>
        <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            <title>Perfil Administrador</title>
            <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
            <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
            <link href="css/style.css" rel="stylesheet" />
            
    
        </head>
        <!-- <body oncontextmenu='return false' class='snippet-body' id="body-pd">  -->
        <div oncontextmenu='return false' class='snippet-body' id="body-pd"> 
            <!-- <header class="header" id="header">
                <section class="container-Admin">
                    <br>
                    <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
                </section>
            </header> -->
            <div class="l-navbar" id="nav-bar">
                <nav class="nav">
                    <p class="nav_link" id="arrowLink"> <i class='bx bx-right-arrow-alt bx-left-arrow-alt nav_logo-icon ' id="arrow" title="Expandir menú"></i> <!--<span class="nav_logo-name">PANEL ADMIN</span> --></p>
                    <div id="navList" class="nav_list"> 
                        <p class="nav_link seccion active" id="ingresos"> <i class='bx bx-line-chart nav_icon'  title="Ingresos y Gastos"></i> <span class="nav_name">Ingresos y Gastos</span> </p>
                        <p class="nav_link seccion" id="ventas"> <i class='bx bxs-doughnut-chart alt-2 nav_icon' title="Ventas"></i> <span class="nav_name">Ventas</span> </p> 
                        <p class="nav_link seccion" id="stock"> <i class='bx bx-grid-alt nav_icon' title="Stock"></i> <span class="nav_name">Stock</span> </p> 
                        <p class="nav_link seccion" id="empleados"> <i class='bx bx-group nav_icon'  title="Empleados"></i> <span class="nav_name">Perfiles Empleados</span> </p> 
                    </div>
                    <div>
                        <a href="index.php" class="nav_link" title="Inicio"><i class='bx bx-home nav_icon'></i> <span class="nav_name">Inicio</span> </a>
                        <a href="../libs/logout.php" class="nav_link" title="Cerrar sesión"> <i class='bx bx-log-out nav_icon'></i> <span class="nav_name">Cerrar Sesión</span> </a>
                    </div>
                </nav>
            </div>
        </div>