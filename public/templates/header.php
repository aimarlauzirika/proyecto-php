<?php
$login=false;
if (isset($_SESSION['nombre'])) {
    $login = true;
}

// $_SERVER['PHP_SELF']; 
$array = explode("/",$_SERVER['PHP_SELF']);
// print_r($array);
// echo $array[count($array)-1];
$url = $array[count($array)-1];
if ($url != 'login.php' && $url != 'signup.php') {
    setcookie('redirigir', $url, 0, "/proyecto-php"); // cookie para volver después del login
}
?>

<!DOCTYPE html>
<html>

<head>
    <!-- Basic -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Site Metas -->
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="images/favicon.png" type="favicon">

    <title> Punto Italia </title>

    <!-- bootstrap core css -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

    <!--owl slider stylesheet -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <!-- nice select  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" integrity="sha512-CruCP+TD3yXzlvvijET8wV5WxxEh5H8P4cmz0RFbKK6FlZ2sYl3AEsKlLPHbniXKSrDdFewhbmBK5skbdsASbQ==" crossorigin="anonymous" />
    <!-- font awesome style -->
    <link href="css/font-awesome.min.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet" />
    <!-- responsive style -->
    <link href="css/responsive.css" rel="stylesheet" />

    

</head>

<body>

    <!-- header section strats -->
    <header class="header_section">
        <div class="container">
            <nav class="navbar navbar-expand-lg custom_nav-container ">
                <a class="navbar-brand" href="index.php">
                    <span>Punto Italia</span>
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class=""> </span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav  mx-auto ">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="carta.php">Carta</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="reservas.php">Reservas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contacto.php">Contacto</a>
                        </li>
                    </ul>
                    <div class="user_option">
                        
                        <?php if(!$login):?>
                            <a href="login.php" class="user_link">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </a>
                        <?php endif?>
                        <?php if($login && $_SESSION['tipoUsuario'] == 0):?>
                            <a href="admin.php" class="header-admin">Admin</a>
                        <?php endif?>
                        <?php if($login):?>
                            <a href="../public/usuario.php" class="header-user">Hola <?= $_SESSION['nombre'] ?></a>
                        <?php endif?>
                        <a href="pedidos.php" class="order_online">Haz tu pedido</a>
                    </div>
                </div>
            </nav>
        </div>
    </header>
