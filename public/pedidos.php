<?php
include_once "../libs/const.php";     
include_once "../libs/funciones.php"; 
include_once "../libs/db.php";

//* Si no hay login redirige
if (!isset($_SESSION['nombre'])) {
    setcookie('redirigir', 'pedidos.php', 0, "/proyecto-php"); // cookie para volver después del login
    header('Location: login.php');
}

//* Si se ha enviado el formulario se procesa
if (isset($_POST['envio'])) {
    include_once "../libs/procesarPedido.php";

    include_once HEADER_DIR; ?>

    <h1 class="h1ExitoPedido">Pedido Realizado Correctamente</h1>

    <?php
    include_once FOOTER_DIR;

    exit; 
    

}

//* Obtener productos de la base de datos

$result = $conn->query("SELECT * FROM producto");

$ensaladas = [];
$pizzas = [];
$carnes = [];
$pastas = [];
$bebidas = [];
while ($row = $result->fetch_assoc()) { 
    switch ($row['tipo']) {
        case 'en':
            $ensaladas[] = $row;
            break;
            
        case 'pi':
            $pizzas[] = $row;
            break;
        
        case 'ca':
            $carnes[] = $row;
            break;
        
        case 'pa':
            $pastas[] = $row;
            break;
        
        case 'be':
            $bebidas[] = $row;
            break;
        
        default:
            break;
    }
}
$carta = ['Ensaladas' => $ensaladas, 'Pizzas' => $pizzas, 'Carnes' => $carnes, 'Pastas' => $pastas, 'Bebidas' => $bebidas];
// debuggear($platos);


include_once HEADER_DIR;

?>        

<main>
    <h1 class="h1_pedido" id="h1">REALIZA TU PEDIDO</h1>
    <div class="carta">
        <nav class="pedido-nav" id="pedidoNav">
            <p class="pedido-nav-option active">Ensaladas</p>
            <p class="pedido-nav-option">Pizzas</p>
            <p class="pedido-nav-option">Carnes</p>
            <p class="pedido-nav-option">Pastas</p>
            <p class="pedido-nav-option">Bebidas</p>
        </nav>
        <form class="form-pedido" id="formPedidos" action="" method="post">
            <div class="secciones"> <!-- Carta -->
                <?php foreach($carta as $tipoPlato => $array):?>
                    <section class="seccion <?=$tipoPlato; echo $tipoPlato!="Ensaladas"?" ocultar":null ?>">
                        
                        <table class="tabla-pedido">
                            <?php foreach ($array as $plato) : ?>  
                                <tr class="tabla-pedido-plato" data-precio= <?= $plato['precio']  ?>>
                                    <td class="td-nombre"><?= $plato['nombre']  ?></td><td><?= number_format($plato['precio'],2,",")?>€</td><td class="td-cantidad">Cantidad: <input type="number" class="input-num-platos" value="0" min="0" max="<?= $plato['stock']?>" name="<?= $plato['codProducto'] ?>" <?= $plato['stock']==0 ? 'disabled' : '' ?>></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </section>
                <?php endforeach; ?>
            </div>
            <label class="envio"> <!-- Método de envío -->
                <select class="select" id="recoger" name="envio">
                    <?php if($_SESSION['tipoUsuario'] == 1){ ?> 
                        <option value="mesa">Para servir en mesa</option>
                    <?php } else { ?>
                        <option value="" disabled selected>Elíge un opción de envío</option>
                        <option value="recoger">Para recoger</option>
                        <option value="domicilio">A domicilio (+2€)</option>
                    <?php } ?>
                </select>
            </label>
            <!-- Resumen del pedido -->
            <div class="resumen" id="resumen">
                <h2>Resumen del pedido</h2>
                <table class="tabla_resumen">
                    <thead>
                        <tr><th>Plato</th><th>Precio</th><th>Cantidad</th></tr>
                    </thead>
                    <tbody class="resumen_tbody" id="tbody"></tbody>
                    <tfoot class="resumen_foot">
                        <tr><td id="total">Total: 0,00€</td><td></td><td></td></tr>
                    </tfoot>
                </table>
                <input class="submit_pedido" type="submit" id="btnSubmit" value="Realizar Pedido">
            </div>
        </form>
    </div>
</main>
<script src="js/pedidos.js"></script>
<?php
include_once FOOTER_DIR;
$conn->close();
?>