<?php include_once "../libs/const.php"; ?>        
<?php include_once "../libs/funciones.php"; ?> 
<?php include_once "../libs/todosProductos.php"; ?>
<?php 
include_once HEADERADMIN_DIR; 
if(!isset($_SESSION['tipoUsuario']) || $_SESSION['tipoUsuario'] != 0){
    header('Location: index.php');
}
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.js"></script>
<div class="admin-body">

    <div class="contenedor_admin" id="contenedorIngresos">
        <h2>Ingresos y Gastos</h2>
        <div class="graficos">
            <div class="chartIngresosGastos">
                <canvas id="canvasIngresosGastos"></canvas>
            </div>
            <div class="chartComensales">
                <canvas id="canvasComensalesDiaSemana"></canvas>
            </div>
        </div>
    </div>
    
    <div class="contenedor_admin ocultar" id="contenedorVentas">
        <h2>Los platos más vendidos</h2>
        <div class="graficos">
            <div class="grupo">
                <div class="chartPlatos">
                    <h3>Ensaladas</h3>
                    <canvas id="canvasEnsaladasMasVendidas"></canvas>
                </div>
                <div class="chartPlatos">
                    <h3>Pizzas</h3>
                    <canvas id="canvasPizzasMasVendidas"></canvas>
                </div>
            </div>
            <div class="grupo">
                <div class="chartPlatos">
                    <h3>Pasta</h3>
                    <canvas id="canvasPastasMasVendidas"></canvas>
                </div>
                <div class="chartPlatos">
                    <h3>Carnes</h3>
                    <canvas id="canvasCarnesMasVendidas"></canvas>
                </div>
            </div>
        </div>
    </div>
    
    <div class="contenedor_admin ocultar" id="contenedorStock">
        <h2 class="stock">Control de stock</h2>
        <table class= "tabla">
            <thead class= "cabecera">
                <tr><th class= "numProd">CÓD.</th><th class="descripcion">DESCRIPCIÓN</th><th>STOCK</th></tr>
            </thead>
            <tbody>
                <?php foreach ($productos as $producto): ?>
                    <?php if($producto['tipo'] == 'ex'){continue;} ?>
                    <tr <?= $producto['stock']<$producto['prodMin']? 'class="pedir"' : null ?>>
                        <td><?php echo $producto['codProducto'];?></td>
                        <td><?php echo $producto['nombre'];?></td>
                        <td><?php echo $producto['stock'];?></td>  
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>

    <div class="contenedor_admin ocultar" id="contenedorEmpleados">

        <h2>Sección administración de empleados</h2>
        
        
        <?php
        $sql = "SELECT nombre, apellido, email, dni FROM usuario WHERE tipo = 1";
        try {
            $resultado = $conn->query($sql);
        } catch (\Throwable $th) {
            echo "<script> console.log('$th')</script>";
        }
        ?>
        <table class="tabla_empleados">
            <thead>
                <tr><th>Nombre</th><th>Apellido</th><th>email</th><th></th></tr>
            </thead>
            <tbody>
                <?php
                while($row = $resultado->fetch_assoc()){
                    $nombre = $row['nombre'];
                    $apellido = $row['apellido'];
                    $email = $row['email'];
                    $dni = $row['dni'];
                    echo "<tr id='$dni'><td>$nombre</td><td>$apellido</td><td>$email</td><td class='div_botones'><div class=\"editar-empleado\">Editar</div><div class=\"eliminar-empleado\">Eliminar</div></td></tr>";
                }
                ?>
            </tbody>
        </table>
        
        <h3 id="tituloForm">Añadir Empleado</h3>
        
        <?php formUsuario(1, "nuevo") ?>
    </div>
    
</div>

<script src="js/admin.js"></script>
<script type='text/javascript' src='js/navAdmin.js'></script>

<?php 
include_once FOOTERADMIN_DIR;
$conn->close();
?>