<?php
include_once "../libs/const.php";
include_once "../libs/funciones.php";
include_once "../libs/db.php";


if (!isset($_SESSION['nombre']) && isset($_POST['email'])) {
	// echo "eiei";
	//* Comprobar que existe email en la base de datos
	$email = $_POST['email'];
	$sql = "SELECT nombre, apellido, email, direccion, password, dni, tipo FROM usuario WHERE email = '$email' LIMIT 1;";
	// echo $sql;
	$result = $conn->query($sql);
	// var_dump($result->num_rows);
	$conn->close();
	$existeUsuario = $result->num_rows;

	if($existeUsuario){
		//* Comprobar la contraseña
		$row = $result->fetch_assoc();
		// print_r($row);
		// exit;
		// if ($row['password'] == $_POST['password']) {
		if (password_verify($_POST['password'], $row['password'])) {
			$_SESSION['nombre'] = $row['nombre'];
			$_SESSION['apellido'] = $row['apellido'];
			$_SESSION['email'] = $row['email'];
			$_SESSION['direccion'] = $row['direccion'];
			$_SESSION['tipoUsuario'] = $row['tipo'];
			$_SESSION['dni'] = $row['dni'];
			$url = $_COOKIE['redirigir'];
			if ($row['tipo'] == 0) {
				header('Location: admin.php');
				exit;
			}
			if(isset($url)){
				if($url == 'logIn.php'){
					header('Location: index.php');
					exit;
				}
				header("Location:$url");
				exit;
			} else {
				header('Location: index.php');
				exit;
			}
		}
	}
}

?> 



<!-- head y header -->
<?php include_once HEADER_DIR;?> 

<section class="body-LogIn">
<br>
<br>
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card-LogIn">
			<div class="card-header">
				<h3>Iniciar Sesión</h3>
			</div>
			<div class="card-body">
				<form action="logIn.php" method="POST">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g fill='#FFFFFF'><path d="M1 5v14l7-7-7-7zm22 14V5l-7 7 7 7zm-11-3l-3.5-3.5L1 20h22l-7.5-7.5L12 16zM1 4l11 11L23 4H1z"></path></g></svg></i>
							</span>
						</div>
						<input type="text" name="email" class="form-control" placeholder="Ingrese su correo" required>
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g fill='#FFFFFF'><path d="M21 10H10.6C9.8 8.2 8 7 6 7c-5 0-5 4-5 5s0 5 5 5c2.2 0 4-1.4 4.7-3.3 0 0 .3-.7 1.3-.7s1 1 1 1 0 1 1 1 1.1-1 1.1-1-.1-1 .9-1 1 1 1 1 0 1 1 1 1-1 1-1 0-1 1-1 1 1 1 1 0 1 1 1 1-1 1-1v-2c0-1.1-.9-2-2-2zM4.1 14c-1 0-1.1-.9-1.1-2s.1-2 1.1-2 1.9.9 1.9 2-.8 2-1.9 2z"></path></g></svg>
							</span>
						</div>
						<input type="password" name="password" class="form-control" placeholder="Ingrese una contraseña" required>
					</div>
					<!-- <div class="row align-items-center remember">
						<input type="checkbox" >Recuérdame
					</div> -->
					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
				</form>
			</div>
			<div class="card-footer">
				<br>
				<div class="d-flex justify-content-center links">
					¿No tienes una cuenta?<br>
					<a href="singup.php">Regístrate</a>
				</div>
				<!--<div class="d-flex justify-content-center">
					<a href="#">¿Olvidaste tu contraseña?</a>
				</div>-->
			</div>
		</div>
	</div>
</div>
<br>
<br>
</section>

  <!-- INICIO DE FOOTER -->
  <?php include_once FOOTER_DIR; ?> 
  <!--FIN DE FOOTER--> 
