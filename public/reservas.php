<?php include_once "../libs/const.php"; ?>        
<?php include_once "../libs/funciones.php"; ?> 
<?php include_once "../libs/db.php"; ?> 
<?php include_once HEADER_DIR; ?> 

<?php
// debuggear($_POST);
//* Si se ha enviado el formulario se procesa
if (isset($_POST['reserva'])) {
    if(isset($_POST['reserva'])){
        $nombre = $_POST["nombre"];
        $email = $_POST["email"];
        $telefono = $_POST["telefono"];
        $fecha = $_POST["fecha"];
        $numComensales = $_POST["numComensales"];
        $servicio = $_POST["servicio"];
        //INSERTA LOS DATOS
        $insertarDatos = "INSERT INTO reserva (nombre, email, telefono,fecha, numComensales, servicio) VALUES ('$nombre', '$email', '$telefono', '$fecha', '$numComensales', '$servicio')";
        /*//MOSTRAR DATOS
        echo $insertarDatos;
        exit;*/
    
        //EJECUTA INSERTAR DATOS
        $ejecutarInsertar = $conn->query($insertarDatos);
        $conn->close();
    
    }

    include_once HEADER_DIR; ?>

    <h1 class="h1ExitoPedido">Reserva Realizada Correctamente</h1>
    <a href="usuario.php" style="display: block;text-align: center; margin-bottom: 40px;">Ver Reservas</a>

    <?php
    include_once FOOTER_DIR;

    exit; 
    
}
?>

<div id="booking" class="section">
    <div class="section-center">
        <div class="container">
            <div class="row">
                <div class="booking-form">
                    <div class="form-header">
                        <h1>Haz tu reserva</h1>
                    </div>
                    <form action="reservas.php" name="formularioreservas" method="POST">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="form-label">Nombre</span>
                                    <input class="form-control" maxlength="40" type="text" name="nombre" id="nombre" required placeholder="Introduce tu nombre">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="form-label">Email</span>
                                    <input class="form-control" type="email" maxlength="60" name="email" id="email"  required placeholder="Introduce tu email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="form-label">Teléfono</span>
                            <input class="form-control" type="tel" minlength="9" maxlength="12" name="telefono" id="telefono"  required placeholder="Introduce tu teléfono">
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <!-- Ajusta el calendario a la fecha del sistema -->
                                    <span class="form-label">Fecha de reserva</span>
                                    <?php $fcha = date("Y-m-d");?>
                                    <input class="form-control" type="date" value='<?php echo $fcha ."' min='".$fcha; ?>'  name="fecha" id="fecha" required>
                                </div>
                            </div>

                            <div class="col-sm-7">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <span class="form-label">Servicio</span>
                                            <select class="form-control" name="servicio" id="servicio" required>
                                                <option value="0" selected disabled>---</option>
                                                <option class="servicioComida" value="0" id="Comida">Comida</option>
                                                <option class="servicioCena" value="1" id="Cena" >Cena</option>                                                
                                            </select>
                                            <span class="select-arrow"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <span class="form-label">Nº Personas</span>
                                            <select name="numComensales" id="numComensales" class="form-control" required>
                                            </select>
                                        
                                            <span class="select-arrow"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-btn">
                                <input type="submit" class="submit-btn" name="reserva" value="Reservar">
                            </div>
                            <a href="usuario.php" class="enlace-mis-reservas">Ver mis Reservas</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- This templates was made by Colorlib (https://colorlib.com) -->

<!-- JAVASCRIPT PARA RESERVA DINAMICA -->
<script src="js/reservasjs.js"></script>

<!-- footer section -->
<?php include_once FOOTER_DIR; ?> 
<!--Fin de Footer--> 
